lz4-java (1.8.0-4) UNRELEASED; urgency=medium

  * Resolve Java 21 FTBFS (Closes: #1053059).
    - d/rules: use java_compat_level variable provided by java-common
      to adjust -source/-target level to the minimum required by the
      default Java.
    - d/control: require updated libmvel-java.

 -- Vladimir Petko <vladimir.petko@canonical.com>  Thu, 23 Nov 2023 15:06:25 +1300

lz4-java (1.8.0-3) unstable; urgency=medium

  * Skip tests on slow architectures to address FTBFS (Closes: #1015885)
    Thank you to Eric Long for the merge request.

 -- tony mancill <tmancill@debian.org>  Sun, 28 Aug 2022 10:34:03 -0700

lz4-java (1.8.0-2) unstable; urgency=medium

  * Upload to unstable

 -- tony mancill <tmancill@debian.org>  Wed, 29 Jun 2022 07:25:58 -0700

lz4-java (1.8.0-1) experimental; urgency=medium

  * New upstream version 1.8.0 (Closes: #1013324)
  * Standards-Version updated to 4.6.1

 -- tony mancill <tmancill@debian.org>  Tue, 28 Jun 2022 22:21:47 -0700

lz4-java (1.5.1-3) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with OpenJDK 17 (Closes: #981852)
  * Standards-Version updated to 4.5.1
  * Switch to debhelper level 13

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 08 Feb 2021 22:00:04 +0100

lz4-java (1.5.1-2) unstable; urgency=medium

  * Team upload.
  * Fixed the backward compatibility with Java 8

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 14 Oct 2019 11:35:29 +0200

lz4-java (1.5.1-1) unstable; urgency=medium

  * Initial release (Closes: #934700)

 -- Saif Abdul Cassim <saif.15@cse.mrt.ac.lk>  Sat, 21 Sep 2019 09:43:31 +0200
